import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainApplication extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		Label lblHeading = new Label("Airport Ride Calcaulator");
		Label lblFrom = new Label("From: ");
		ObservableList<String> options = FXCollections.observableArrayList("Cestar College", "Brampton");
		ComboBox<String> comboFrom = new ComboBox<>(options);
		CheckBox cbLuggage = new CheckBox("Extra Luggage?");
		CheckBox cbPets = new CheckBox("Pets");
		CheckBox cbUse407 = new CheckBox("Use 407 ETR?");
		CheckBox cbAddTip = new CheckBox("Add Tip?");
		Button btn = new Button("CALCULATE");
		Label lblResult = new Label("");
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// Total Fare = [(Base Fare + Additional Charges) + tax] + tip
				// Base fare = Cestar College → YYZ: $51 + 13% HST
				// Brampton → YYZ $38 + 13% HST

				double total = 0;
				double base = 0;
				int kms = 0;
				if ("Brampton".contentEquals(comboFrom.getSelectionModel().getSelectedItem())) {
					base = 38;
					kms = 5;
				} else {
					base = 51;
					kms = 3;
				}

				total += base;

				if (cbLuggage.isSelected()) {
					total += 10;
				}

				if (cbPets.isSelected()) {
					total += 6;
				}

				if (cbUse407.isSelected()) {
					total += (0.25 * kms);
				}

				// Calculate and add Tax
				total += total * 0.13;

				if (cbAddTip.isSelected()) {
					double tip = total * 0.15;
					total += tip;
				}

				lblResult.setText("The total fare is: " + total);
			}
		});

		VBox root = new VBox(lblHeading, lblFrom, comboFrom, cbLuggage, cbPets, cbUse407, cbAddTip, btn, lblResult);

		root.setSpacing(10);

		root.setPadding(new Insets(10, 50, 50, 50));

		primaryStage.setScene(new Scene(root, 500, 300));
		primaryStage.setTitle("Airport Ride Calculator");
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}